package common.function;

import java.time.LocalDate;
import java.util.Scanner;

import pojo.Books;
import pojo.Copies;
import pojo.Payments;
import pojo.Users;

public class Input {

	static Scanner sc = new Scanner(System.in);
	
	static LocalDate date = LocalDate.now();

	public static void acceptRecord(Users u) {
		System.out.print("Name : ");
		u.setName(sc.nextLine());
		System.out.print("Email : ");
		u.setEmail(sc.nextLine());
		System.out.print("Phone :  ");
		u.setPhone(sc.nextLine());
		System.out.print("Pass : ");
		u.setPassword(sc.nextLine());
	
	}

	public static String inputPassWord() {
		System.out.print("Enter New Password : ");
		return sc.nextLine();
	}
	public static void updateRecord(Users u) {
		System.out.print("Name : ");
		u.setName(sc.nextLine());
		System.out.print("Email : ");
		u.setEmail(sc.nextLine());
		System.out.print("Phone :  ");
		u.setPhone(sc.nextLine());
	}
   
	public static void inputBookRecord(Books b) {
		System.out.print("Book Name : ");
		b.setName(sc.nextLine());
		System.out.print("Book Author : ");
		b.setAuthor(sc.nextLine());
		System.out.print("Subject : ");
		b.setSubject(sc.nextLine());
		System.out.print("isbn : ");
		b.setIsbn(sc.nextLine());
		System.out.print("Price : ");
		b.setPrice(sc.nextFloat());
	}

	public static void enterBook(Books b) {
	System.out.print("Book Name : ");
	b.setName(sc.nextLine());
	b.setName(sc.nextLine());	
	}
  public static void enterCopy(Copies copy) {
		System.out.print("Book Id : ");
		copy.setBookId(sc.nextInt());
		sc.nextLine();
		System.out.print("Rack : ");
		copy.setRack(sc.nextLine());
	}

	public static int getMemberIdFromUser() {
		System.out.print("Enter Member Id : ");
		   return sc.nextInt();
		
	}
	
	public static String enterRack() {
	     System.out.print("Enter new rack");
		String abc =  sc.nextLine();
         abc =  sc.nextLine();
		 return abc;
	}
	
	public static void inputToAddLibraryMember(Users u) {
		System.out.print("Name : ");
		u.setName(sc.nextLine());
		System.out.print("Email : ");
		u.setEmail(sc.nextLine());
		System.out.print("Phone :  ");
		u.setPhone(sc.nextLine());
	}

	public static Payments inputPaymentRecord(Payments payment) {
	    System.out.print("User Id :"); 
	   payment.setUserId(sc.nextInt());
	    System.out.print("Amount : ");
	    payment.setAmount(sc.nextFloat());
	    System.out.print("type : ");
	    payment.setType(sc.nextLine());
	    payment.setType(sc.nextLine());
	   payment.setTransactionTime(payment.getDat().toString());
	   return payment;
	}

	public static int copyIdToReturn() {
		System.out.print("Enter Copy ID : ");
		return sc.nextInt();
	}

	public static LocalDate inputDate() {
		System.out.print("Enter Date in the form of yyyy--mm--dd   :  ");
		return LocalDate.parse(sc.nextLine());
	}
	
}
