package librerian;

import java.sql.SQLException;

import Dao.BooksDao;
import Dao.CopiesDao;
import Dao.PaymentDao;
import Dao.UsersDao;
import MENU.Program;
import common.function.CrediantialCheck;
import common.function.Input;
import pojo.Books;
import pojo.Copies;
import pojo.Payments;
import pojo.Users;

public class Librerian {

	public static void librerian() throws SQLException {
		int choice = 0;
		UsersDao ud = new UsersDao();
		Users u = new Users();
		Books b = new Books();
		Payments payment = new Payments();
		BooksDao book = new BooksDao();
		Copies copy = new Copies();
		CopiesDao cd = new CopiesDao();
		PaymentDao paymentd = new PaymentDao();
		while ((choice = Program.librerianMenu()) != 0) {
			int i = 0;
			switch (choice) {
			case 1:
				Input.updateRecord(u);
				i = ud.updateProfile(u, CrediantialCheck.id);
				if (i > 0)
					System.out.println("Your Profile is updated");
				break;
			case 2:
				i = ud.updatePassword(CrediantialCheck.id);
				if (i > 0)
					System.out.println("Your Password Changed");
				break;
			case 3:
				Input.enterBook(b);
				book.searchBook(b);
				break;
			case 4:
				cd.displayAvailability();
				break;
			case 5:
				Input.inputBookRecord(b);
				i = book.addBook(b);
				if (i > 0)
					System.out.println("Book added ");
				break;
			case 6:
				 Input.enterCopy(copy);
				i = cd.addCopy(copy);
				if (i > 0)
					System.out.println("Copy added ");
				break;
			case 7:
				int id = Input.getMemberIdFromUser();
				if(paymentd.checkPayment(id)<0) {
					System.out.println("Sorry you have not paid the fees ");
				}else {
					Input.enterBook(b);
					book.searchBook(b);
					if(cd.issueCopy(b,id)>0)
						System.out.println("Book Issued ");
				}
				break;
			case 8:
				  if(cd.returnCopy()>0)
					  System.out.println("Book returned ");
				break;
			case 9:book.listIssuedBooks();
				break;
			case 10:
				Input.enterBook(b);
				int id1 = book.searchBook(b);
				book.editBook(b,id1);
				break;
			case 11:
				cd.changerack(Input.getMemberIdFromUser());
				if (i > 0)
					System.out.println("Rack Changed  ");
				break;
			case 12:
				Input.inputToAddLibraryMember(u);
				i = ud.addMember(u);
				if (i > 0)
					System.out.println("Member added ");
				break;
			case 13://take payment
				paymentd.addPaymentRecord(	Input.inputPaymentRecord(payment));
				break;
			case 14:paymentd.paymentHistory();
				break;

			}
		}
	}


}
