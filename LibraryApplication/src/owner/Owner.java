package owner;


import java.sql.SQLException;

import Dao.CopiesDao;
import Dao.PaymentDao;
import Dao.UsersDao;
import MENU.Program;
import common.function.CrediantialCheck;
import common.function.Input;
import pojo.Users;;
public class Owner {

	public static void owner() throws SQLException {
		int choice = 0;
		UsersDao ud = new UsersDao();
		PaymentDao paymentd = new PaymentDao();
		Users u = new Users();
		CopiesDao cd = new CopiesDao();
		while ((choice = Program.ownerMenu()) != 0) {
			switch (choice) {
			case 1:// edit profile
				Input.updateRecord(u);
				break;
			case 2:// update password
				 ud.updatePassword(CrediantialCheck.id);
				break;
			case 3:// appoint librarian
				Input.acceptRecord(u);
				ud.appointLibrerian(u);
				break;
			case 4:cd.subjectWiseCopiesReport();
				break;
			case 5:cd.bookWiseCopiesReport();
				break;
			case 6:paymentd.feesFineReportGeneration();
				break;

			}
		}
	}

}
