package pojo;

public class Copies {
   private int id;
   private int bookId;
   private String rack;
   private String Status;
	public Copies() {
		
	}
	public Copies(int id, int bookId, String rack, String status) {
		super();
		this.id = id;
		this.bookId = bookId;
		this.rack = rack;
		Status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getRack() {
		return rack;
	}
	public void setRack(String rack) {
		this.rack = rack;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	@Override
	public String toString() {
		
    return "Copies [id=" + id + ", bookId=" + bookId + ", rack=" + rack + ", Status=" + Status + "]";
	}
	
 
}
