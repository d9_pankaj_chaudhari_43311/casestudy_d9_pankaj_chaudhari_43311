package pojo;

public class IssuceRecord {
     private int id;
     private int copyId;
     private int memberId;
     private String issueDate;
     private String returnDueDate;
     private String returnDate;
     private String fineAmount;
     
	public IssuceRecord() {
		super();
	}

	public IssuceRecord(int id, int copyId, int memberId, String issueDate, String returnDueDate, String returnDate,
			String fineAmount) {
		this.id = id;
		this.copyId = copyId;
		this.memberId = memberId;
		this.issueDate = issueDate;
		this.returnDueDate = returnDueDate;
		this.returnDate = returnDate;
		this.fineAmount = fineAmount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCopyId() {
		return copyId;
	}

	public void setCopyId(int copyId) {
		this.copyId = copyId;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getReturnDueDate() {
		return returnDueDate;
	}

	public void setReturnDueDate(String returnDueDate) {
		this.returnDueDate = returnDueDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getFineAmount() {
		return fineAmount;
	}

	public void setFineAmount(String fineAmount) {
		this.fineAmount = fineAmount;
	}

	@Override
	public String toString() {
		return "IssuceRecord [id=" + id + ", copyId=" + copyId + ", memberId=" + memberId + ", issueDate=" + issueDate
				+ ", returnDueDate=" + returnDueDate + ", returnDate=" + returnDate + ", fineAmount=" + fineAmount
				+ "]";
	}
 
}
