package pojo;

import java.time.LocalDate;

public class Payments {
    private int id;
    private int userId;
    private float amount;
    private String type;
    private String transactionTime;
    private String nextPaymentDueDate;
    private LocalDate dat = LocalDate.now();
    public LocalDate getDat() {
		return dat;
	}
	public void setDat(LocalDate dat) {
		this.dat = dat;
	}
	public Payments() {
		
	}
	public Payments(int id, int userId, float amount, String type, String transactionTime, String nextPaymentDueDate) {
		this.id = id;
		this.userId = userId;
		this.amount = amount;
		this.type = type;
		this.transactionTime = transactionTime;
		this.nextPaymentDueDate = nextPaymentDueDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}
	public String getNextPaymentDueDate() {
		return nextPaymentDueDate;
	}
	public void setNextPaymentDueDate(String nextPaymentDueDate) {
		this.nextPaymentDueDate = nextPaymentDueDate;
	}
	@Override
	public String toString() {
		return "Payments [id=" + id + ", userId=" + userId + ", amount=" + amount + ", type=" + type
				+ ", transactionTime=" + transactionTime + ", nextPaymentDueDate=" + nextPaymentDueDate + "]";
	}
	
    
    
}
