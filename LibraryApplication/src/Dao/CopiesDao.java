package Dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.time.temporal.ChronoUnit;
import Connection.GetConnection;
import common.function.Constants;
import common.function.CrediantialCheck;
import common.function.Input;
import pojo.Books;
import pojo.Copies;


public class CopiesDao  {
	static String sql = null;
	private Connection connection = null;
	private Statement statement = null; 
	LocalDate date = LocalDate.now();
	IssueRecordDao IR = new IssueRecordDao();
	public CopiesDao() throws SQLException {
		connection = GetConnection.getConnection();
		 statement = connection.createStatement();
	}

	public int addCopy(Copies copy) throws SQLException {
		sql = "INSERT into library_system.copies (book_id,rack,status)VALUES('" + copy.getBookId() + "','"
				+ copy.getRack() + "','" +Constants.AVAILABLE + "')";
		return statement.executeUpdate(sql);
	}

	public void displayAvailability() throws SQLException {
		sql = "SELECT * FROM copies";
		ResultSet rs = statement.executeQuery(sql);
		List<Copies> copyList = new ArrayList<>();
		while (rs.next()) 
			copyList.add(
					new Copies(rs.getInt("id"), rs.getInt("book_id"), rs.getString("rack"), rs.getString("status")));
		copyList.forEach(System.out::println);

	}

	public int changerack(int copyIdFromUser) throws SQLException {
		String rack = Input.enterRack();
		sql = "UPDATE copies SET rack = '" + rack + "' WHERE id ='" + copyIdFromUser + "'";
		return statement.executeUpdate(sql);
	}

	public int issueCopy(Books b, int id) throws SQLException {
		int book_id = 0;
		int copy_id = 0;
		sql = "SELECT * FROM books WHERE name ='" + b.getName() + "'";
		ResultSet rst = statement.executeQuery(sql);
		rst.next();
		book_id = rst.getInt("id");

		sql = "SELECT id FROM copies WHERE book_id ='" + book_id + "' AND status ='" + Constants.AVAILABLE + "'";
		ResultSet rst2 = statement.executeQuery(sql);

		if (rst2.next())
			copy_id = rst2.getInt("id");
		else
			System.out.println("Copy not available");

		sql = "UPDATE copies SET status = '" + Constants.ISSUED + "' WHERE id ='" + copy_id + "'";
	
		     int temp = statement.executeUpdate(sql);

		if (temp > 0) {
			sql = "INSERT into issue_record(copy_id,member_id, issue_date,return_duedate)values('" + copy_id + "','"
					+ id + "','" + date.toString() + "','" + date.plusDays(7).toString() + "')";
			statement.executeUpdate(sql);
		}

		return temp;
	}
	
	public int returnCopy() throws SQLException {
		int memberId = Input.getMemberIdFromUser();	
		IR.displayIssueRecord(memberId);
		int CopyId =Input.copyIdToReturn();

		sql = "UPDATE issue_record SET return_date = '" + date.toString() + "' WHERE copy_id = '" + CopyId + "'";
		statement.executeUpdate(sql);
		LocalDate dueDate = null;
		sql = "SELECT * FROM issue_record WHERE copy_id = '" + CopyId + "'";
		try (ResultSet rst2 = statement.executeQuery(sql)) {
			if (rst2.next()) {
				dueDate = LocalDate.parse(rst2.getString("return_duedate"));
			}
		}
		long days = ChronoUnit.DAYS.between(dueDate,date);

		if (days <= 7) {
			CrediantialCheck.finaAmount = 0;
		} else {
			CrediantialCheck.finaAmount = days * 5;
		}
		
		sql = "UPDATE issue_record SET fine_amount = '" + CrediantialCheck.finaAmount + "' WHERE copy_id = '" + CopyId
				+ "'";
		statement.executeUpdate(sql);
		sql = "UPDATE copies SET status = '" + Constants.AVAILABLE + "' WHERE id = '" + CopyId + "'";
		return statement.executeUpdate(sql);

	}

	public void subjectWiseCopiesReport() throws SQLException {
		 sql = "select subject,count(*) as no_of_copies from books as b\r\n" + 
		 		"inner join copies AS c  on b.id =c.book_id\r\n" + 
		 		"GROUP by subject;\r\n" + 
		 		" ";
		 ResultSet rs = statement.executeQuery(sql);
		  while(rs.next()) {
			  System.out.println("Subject : "+rs.getString(1)+" "+" No fo copies : "+rs.getString(2));
		  }
	}

	public void bookWiseCopiesReport() throws SQLException {
		  ResultSet rs = statement.executeQuery("SELECT b.id, b.name, bc.status,Count(*) AS Count FROM copies bc INNER JOIN books b ON b.id = bc.book_id GROUP BY bc.status,b.subject,b.id;");
		  System.out.printf("%-20s%-40s%-20s%-10s%n", "Book id", "Book Name", "status", "No of copies");
          while (rs.next()) {
				System.out.printf("%-20d%-40s%-20s%-10d%n", rs.getInt("id"), rs.getNString("name"),
						rs.getNString("status"), rs.getInt("Count"));
          }
    }
	
}
