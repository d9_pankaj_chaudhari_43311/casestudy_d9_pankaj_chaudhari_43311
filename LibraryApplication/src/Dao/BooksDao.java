package Dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Connection.GetConnection;
import common.function.Input;
import pojo.Books;

public class BooksDao {
	static String sql = null;
	private Connection connection = GetConnection.getConnection();
	Statement statement = connection.createStatement();

	public BooksDao() throws SQLException {

	}

	public int addBook(Books b) throws SQLException {
		sql = "INSERT into library_system.books (name,author,subject,price,isbn)VALUES('" + b.getName() + "','"
				+ b.getAuthor() + "','" + b.getSubject() + "','" + b.getPrice() + "','" + b.getIsbn() + "')";
		return statement.executeUpdate(sql);
	}

	public int searchBook(Books b) throws SQLException {
		sql = "SELECT * FROM books WHERE name  LIKE '%" + b.getName() + "%'";
		ResultSet rst = statement.executeQuery(sql);
		if (rst.next()) {
			b.setId(rst.getInt("id"));
			b.setName(rst.getString("name"));
			b.setAuthor(rst.getString("author"));
			b.setSubject(rst.getString("subject"));
			b.setPrice(rst.getFloat("price"));
			System.out.println(b.toString());
		} else {
			System.out.println("Book Not available");
		}
		if(rst.next())
		    return rst.getInt("id");
		else
			return 0;
	}

	public int editBook(Books b, int id) throws SQLException {
		System.out.println("Enter New updated book information : ");
		Input.inputBookRecord(b);
		sql = "UPDATE books SET name = '" + b.getName() + "', author = '" + b.getAuthor() + "',subject = '"
				+ b.getSubject() + "',price='" + b.getPrice() + "',isbn='" + b.getIsbn() + "' WHERE id=" + id + "";
		return statement.executeUpdate(sql);
	}

	public void listIssuedBooks() throws SQLException {
		sql = "select b.id,b.name,b.author,b.subject,b.price, b.isbn,c.id,i.id,i.member_id,i.issue_date,"
				+ "i.return_duedate,i.return_date,i.fine_amount from books as b inner join copies as c on"
				+ " b.id = c.book_id inner join issue_record as i on c.id = i.copy_id where status = "
				+ "'ISSUED' AND  return_date IS NULL";
          int flag = 0;
		ResultSet rs = statement.executeQuery(sql);
		while (rs.next()) {
			flag = 1;
			System.out.println("{ " + "Book Id : " + rs.getInt(1) + "  " + "Book Name : " + rs.getString(2) + "  "
					+ " Price : " + "  " + rs.getInt("price") + " " + " " + "Copy Id : " + rs.getString("c.id") + " "
					+ " Issue Id : " + rs.getString("i.id") + " " + " Member Id : " + rs.getString("i.member_id") + " "
					+ "issue_date: " + rs.getString("i.issue_date") + " " + " return_duedate : "
					+ rs.getString("i.return_duedate"));
		}
		if(flag == 0)
			System.out.println("There are No Issued books ");

	}

}
