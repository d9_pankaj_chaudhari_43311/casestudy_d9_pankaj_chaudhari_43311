package Dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Connection.GetConnection;
import pojo.IssuceRecord;

public class IssueRecordDao {
	public IssueRecordDao() throws SQLException {
		
	}

	public void displayIssueRecord(int memberId) throws SQLException {
		try (Connection connection = GetConnection.getConnection();
				Statement statement = connection.createStatement()) {
			String sql = "SELECT * FROM issue_record WHERE member_id = '" + memberId + "' AND return_date IS NULL";
			List<IssuceRecord> bookList = new ArrayList<>();
			try (ResultSet rs = statement.executeQuery(sql);) {
				while (rs.next())
					bookList.add(new IssuceRecord(rs.getInt("id"), rs.getInt("copy_id"), rs.getInt("member_id"),
							rs.getString("issue_date"), rs.getString("return_duedate"), rs.getString("return_date"),
							rs.getString("fine_amount")));
				bookList.forEach(System.out::println);
			}
		}
	}
}
