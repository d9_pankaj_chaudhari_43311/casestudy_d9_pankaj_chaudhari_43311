package Dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import Connection.GetConnection;
import common.function.Input;
import pojo.Payments;

public class PaymentDao {
	static String sql = null;
	private Connection connection = GetConnection.getConnection();
	Statement statement = connection.createStatement();

	public PaymentDao() throws SQLException {
		super();
	}

	public int addPaymentRecord(Payments p) throws SQLException {
		LocalDate nextDate = calculateNextPaymentDueDate(p);
		String sql = "INSERT INTO payments(user_id,amount,type,transaction_time,next_payment_due_date) VALUES('"
				+ p.getUserId() + "','" + p.getAmount() + "','" + p.getType() + "','" + p.getDat().toString() + "','"
				+ nextDate.toString() + "')";

		return statement.executeUpdate(sql);

	}

	public static LocalDate calculateNextPaymentDueDate(Payments p) {

		return p.getDat().plusDays(30);
	}

	public float checkPayment(int id) throws SQLException {
		sql = "SELECT amount FROM payments WHERE user_id ='" + id + "'";
		ResultSet rst = statement.executeQuery(sql);
		if(rst.next())
		    return rst.getFloat("amount");
		else
		{
			return -1;
		}
		
	}

	public void paymentHistory() throws SQLException {
      int id =	Input.getMemberIdFromUser();
      sql = "SELECT * FROM payments WHERE user_id = '" + id + "'";
      List<Payments> paymentList = new ArrayList<>();
		try (ResultSet rs = statement.executeQuery(sql);) {
			while (rs.next())
				paymentList.add(new Payments(rs.getInt("id"), rs.getInt("user_id"), rs.getFloat("amount"),
						rs.getString("type"), rs.getString("transaction_time"), rs.getString("next_payment_due_date")
					 ));
			paymentList.forEach(System.out::println);
		}
	}

	public void feesFineReportGeneration() throws SQLException {
		System.out.println("Enter start and last with specified format :) ");
		LocalDate startDate = Input.inputDate();
		LocalDate lastDate = Input.inputDate();
		double totalFineAmount = 0;
		double totalFeesAmount = 0;
		sql = "SELECT * FROM payments";
		ResultSet rs = statement.executeQuery(sql);

		while (rs.next()) {
			LocalDate temp = LocalDate.parse(rs.getString("transaction_time"));
			String type = rs.getString("type");
			if (temp.isAfter(startDate) && temp.isBefore(lastDate)) {
					if (type.equals("fee"))
						totalFeesAmount = totalFeesAmount + rs.getFloat("amount");
					else if (type.equals("fine"))		
					    totalFineAmount = totalFineAmount + rs.getFloat("amount");
			} else if ((temp.compareTo(startDate) == 0)) {
				if (type.equals("fee"))
					totalFeesAmount = totalFeesAmount + rs.getFloat("amount");
				else if (type.equals("fine"))		
				    totalFineAmount = totalFineAmount + rs.getFloat("amount");
			} else if ((temp.compareTo(lastDate) == 0)) {
				if (type.equals("fee"))
					totalFeesAmount = totalFeesAmount + rs.getFloat("amount");
				else if (type.equals("fine"))		
				    totalFineAmount = totalFineAmount + rs.getFloat("amount");

			}

		}
		System.out.println("from date (yyyy--mm--dd ) : "+startDate);
		System.out.println("to date   (yyyy--mm--dd)) : "+lastDate);
		System.out.println("Total fees Amount Collected : "+totalFeesAmount);
		System.out.println("Total fine Amount Collected : "+totalFineAmount);
	
	}
}
