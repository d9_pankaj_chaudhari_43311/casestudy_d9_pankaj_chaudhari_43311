package member;

import java.sql.SQLException;

import Dao.BooksDao;
import Dao.CopiesDao;
import Dao.UsersDao;
import MENU.Program;
import common.function.CrediantialCheck;
import common.function.Input;
import pojo.Books;
import pojo.Users;

public class Members {
        
	public static void members() throws SQLException {
		Users u = new Users();
		UsersDao ud = new UsersDao();
		Books b = new Books();
		BooksDao book = new BooksDao();
		CopiesDao cd = new CopiesDao();
		int choice = 0;
		while ((choice = Program.memberMenu()) != 0) {
			int i = 0;
			switch (choice) {
			case 1:Input.updateRecord(u);
			i = ud.updateProfile(u, CrediantialCheck.id);
			if (i > 0)
				System.out.println("Record updated");
			break;
			case 2:
				i = ud.updatePassword(CrediantialCheck.id);
			if (i > 0)
				System.out.println("Password Changed");
				break;
			case 3:
				Input.enterBook(b);
			    book.searchBook(b);
				break;
			case 4:	cd.displayAvailability();
				break;
	        } 

	 }

}
}