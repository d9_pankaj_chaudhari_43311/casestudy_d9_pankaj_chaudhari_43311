package MENU;

import java.util.Scanner;

import common.function.CrediantialCheck;

public class Program {
   
	static Scanner sc = new Scanner(System.in);
	
	public static int mainMenu() {
		System.out.println("Press :");
		System.out.println("0.SIGN OUT");
		System.out.println("1.SIGN IN");
		System.out.println("2.SIGN UP");

		return sc.nextInt();
	}

	public static int ownerMenu() {
		System.out.println();
		System.out.println("Hey, "+CrediantialCheck.displayName+" Welcome..!!");
		System.out.println("Press : ");
		System.out.println("0.SIGN OUT");
		System.out.println("1.EDIT PROFILE");
		System.out.println("2.CHANGE PASSWORD");
		System.out.println("3.APPOINT LIBRERIAN");
		System.out.println("4.SUBJECT COPIES REPORT");
		System.out.println("5.BOOKWISE COPIES REPORT");
		System.out.println("6.FEES / FINE REPORT");
		return sc.nextInt();
	}

	public static int librerianMenu() {
		System.out.println();
		System.out.println("Hey, "+CrediantialCheck.displayName+" Welcome..!!");
		System.out.println("Press : ");
		System.out.println("0.sign out");
		System.out.println("1.edit profile");
		System.out.println("2.change password");
		System.out.println("3.find book by name");
		System.out.println("4.check book availability");
		System.out.println("5.add new book");
		System.out.println("6.add new copy");
		System.out.println("7.issue book copy");
		System.out.println("8.return book copy");
		System.out.println("9.list issued books");
		System.out.println("10.edit book");
		System.out.println("11.change rack");
		System.out.println("12.add member");
		System.out.println("13.take payment");
		System.out.println("14.payment history");
		return sc.nextInt();
	}

	public static int memberMenu() {
		System.out.println();
		System.out.println("Hey, "+CrediantialCheck.displayName+" Welcome..!!");
		System.out.println("Press : ");
		System.out.println("0.sign out");
		System.out.println("1.edit profile");
		System.out.println("2.change password");
		System.out.println("3.find book by name");
		System.out.println("4.check book availability");
		System.out.println("5.list issued books");
		System.out.println("6.payment history");
		return sc.nextInt();		
	}

}
