package Connection;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import test.Test;

public class GetConnection {
	private static Properties p;
	static {
		try {
			p = new Properties();
			InputStream inputStream = Test.class.getClassLoader().getResourceAsStream("config.properties");
			p.load(inputStream);
			Class.forName(p.getProperty("DRIVER"));
		}catch (Exception cause) {
			throw new RuntimeException( cause );
		}
	}
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(p.getProperty("URL"), p.getProperty("USER"), p.getProperty("PASSWORD"));
	}

}
